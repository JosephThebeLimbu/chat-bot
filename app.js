var dotenv = require("dotenv");
dotenv.config();
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
// const loggerModule = require('./logger/main');
// const {accessLogger, systemLogger, errorLogger} = loggerModule;
var morgan = require("morgan");
// var redis = require("redis");

var indexRouter = require("./routes/index");
var forexRouter = require("./routes/forex");
var locationRouter = require("./routes/location");
var usersRouter = require("./routes/users");
const getOrganization = require("./routes/getorganization");
var mailRouter = require("./routes/mail");
var chatHistoryRouter = require("./routes/chatHistory");
var leadsRouter = require("./routes/leads");
const expressSanitizer = require("express-sanitizer");
var localData = require("./localDataJson/localrundata");
var problemRouter = require("./routes/problem");
var emitedText = require("./localDataJson/Configure");
var nccBranch = require("./nccBranch/nccBankBrannch");
var imeMode = require("./routes/imeMode");
let imeRivival = require("./routes/imeRivival");

var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server);
const fetch = require("node-fetch");
// var unhandleMSG = require("./localDataJson/unhandleMSG");
var unhandleMSG = "have not any response";
app.use(function (req, res, next) {
  res.io = io;
  next();
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,PATCH");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressSanitizer());
app.use(express.static(path.join(__dirname, "public")));

const systemLog = {
  write: function (message, encoding) {
    // systemLogger.log({
    //   level: "info",
    //   message: {
    //     title: message
    //   }
    // });
  },
};
app.use(morgan("combined", { stream: systemLog }));
app.use("/", indexRouter);
app.use("/rest/v1/location", locationRouter);
//app.use('/rest/v1/forex', forexRouter);
app.use("/rest/v1/users", usersRouter);
app.use("/rest/v1/nccBranch", nccBranch);
//app.use("/rest/v1/mail", mailRouter);
app.use("/rest/v1/chat", chatHistoryRouter);
app.use("/rest/v1/leads", leadsRouter);
app.use("/rest/v1/insurance/ime/agent", imeMode);
app.use("/rest/v1/ime/revival", imeRivival);
//app.use("/rest/v1/problem", problemRouter);
app.use("/rest/v1/Organization", getOrganization);
app.use("/bot", express.static(path.join(__dirname)));
app.use(
  "/chat",
  express.static(path.join(__dirname, "public", "chatHistory", "loginTemplate"))
);
app.use(
  "/chat/converse",
  express.static(path.join(__dirname, "public", "chatHistory", "chatTemplate"))
);
app.use(
  "/pushNotification",
  express.static(path.join(__dirname, "public", "pushNotification", "push"))
);
var checkUserPersists = function (id, source) {
  let url = `${process.env.SOCKET_PROTOCOL}://${process.env.DASHBOARD_SERVER}:${process.env.DASHBOARD_PORT}/${process.env.BASEPATH}/visitors/userId?userId=${id}&access_token=${process.env.ADMIN_TOKEN}`;
  let organizationId = process.env.ORGANIZATION_ID;
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      source: source,
      user_id: id,
      organizationId: organizationId,
    }),
  });
};

s = [];
io.sockets.on("connection", function (socket) {
  socket.on("disconnect", function () {
    let messageJSON = {
      title: null,
      ipAddress: socket.handshake.headers["x-forwarded-for"],
      isConnected: "disconnected",
      socketID: socket.id,
      userID: null,
      botName: "NepalBank-bot",
      source: "web",
      context: "live-chat",
    };

    // accessLogger.log({
    //   level: "info",
    //   message: messageJSON
    // });
  });

  socket.on("join", function (room) {
    socket.join(room);
    if (room == "Agent") {
    } else {
      setIntervention(room);
    }
  });

  socket.on("agent_join", function (room) {
    socket.join(room);
  });

  socket.on("alert", function (message) {
    io.to("Agent").emit("agent_alert", message);
  });

  /* if agent disconnected during conversation 
  socket.on("user_disconnected", function (message, space) {
    io.to(space).emit("message_received", message);
  });*/

  socket.on("agent_send", function (message, space, source, senderId) {
    io.to(space).emit("message_received", message);
    if (source == "fb") {
      postFbMessage(message, space, "outgoing", senderId)
        .then((data) => data.json())
        .then((data) => {
          debugger;
        })
        .catch((err) => {
          debugger;
        });
    } else {
      // for message persistence.
      postMessage(message, space, "outgoing", senderId);
    }
  });

  socket.on("user_message", function (message, userID) {
    let messageJSON = {
      title: message,
      ipAddress: socket.handshake.headers["x-forwarded-for"],
      isConnected: "connected",
      socketID: socket.id,
      userID: userID,
      botName: "NepalBank-bot",
      source: "web",
      context: "live-chat",
    };
    // accessLogger.log({
    //   level: "info",
    //   message: messageJSON
    // });
    checkUserPersists(userID, "web")
      .then((res) => res.json())
      .then((data) => {
        debugger;
        return postMessage(message, userID, "incoming", "human");
      })
      .then((data) => {
        debugger;
      })
      .catch((err) => {
        messageJSON.title = err.message;
        // errorLogger.log({
        //   level: "error",
        //   message: messageJSON
        // });
      });
    ///new addition for testing purpose only-------------------------------------------------------------------------------------------------------------------
    ///---------///---------///---------///---------///---------///---------///---------///---------///---------
     loan_module = {
  "type": "formMessageSection",
  "exitMSG": "Oops !! you quitted midway during rivival change.",
  "form": {
    "elements": [
      {
        "order": "text",
        "title": "Your Policy Last Name Please?",
        "type": "textbox",
        "placeholder": "Please Type Your Last Name",
        "utterances": [
          {
            "message": "Please authenticate yourself by answering some questions to revive the policy ."
          }
        ],
        "validation": {
          "type": "name",
          "error": "Please enter valid last  name."
        },
        "label": "LastName"
      },
      {
        "order": "number",
        "title": "Enter Policy Number which you want to revive ?",
        "type": "textbox",
        "placeholder": "Enter Policy Number",
        "validation": {
          "type": "PolicyNumber",
          "error": "Invalid!! Provide 9 digit Policy Number"
        },
        "label": "PolicyNumber"
      },
      {
        "order": "text",
        "title": "Specify Assured DOB",
        "placeholder": "Please Enter Assured Date",
        "type": "date",
        "label": "DateOfBirth"
      },
      {
        "order": "cancle",
        "title": " Please click Submit button to proceed further and Cancel to quit ",
        "type": "submitbutton",
        "placeholder": "Enter your email",
        "button": [
          {
            "submit": "Submit",
            "type": "submit"
          },
          {
            "submit": "Cancel",
            "type": "cancel"
          }
        ]
      }
    ]
  },
  "post": "/rest/v1/insurance/ime/agent?type=policy_dueinfo&action=email&emailfor=LOAN"
}

//head branch local data
 head_branch= {
  "type": "contact",
  "title": "Dear customer, you can contact us at:  ",
  "data": {
    "subtitle": "IME Life Insurance ",
    "button": {
      "title": "Branch Details",
      "link": "https://imelifeinsurance.com/IME-Network.aspx"
    },
    "info": [
      {
        "key": "Address",
        "value": "3rd Floor, Hathway Complex, Lainchaur, Kathmandu",
        "icon": "<i class='fas fa-map-marker-alt'></i>"
      },
      {
        "key": "Phone",
        "value": "977-01-4024071",
        "icon": "<i class='fas fa-phone-alt'></i>"
      },
      {
        "key": "Fax",
        "value": "977-01-4024071",
        "icon": "<i class='fas fa-fax'></i>"
      },
      {
        "key": "Toll-Free Number",
        "value": "977-01-4024071",
        "icon": "<i class='fas fa-phone-alt'></i>"
      },
      {
        "key": "Email",
        "value": "info@imelifeinsurance.com",
        "icon": "<i class='fas fa-envelope'</i>"
      }
    ]
  }
}
  
 
    ///-----------------------------------------------------------------------------------------------------------
    if (message === "claim") {
      socket.emit("message_received", localData.claim);
    } else if (message === "policy status") {
      socket.emit("message_received", localData.policy_status);
    } else if (message === "downline") {
      socket.emit("message_received", localData.AgentDownlineBusiness);
    } else if (message === "loan") {
      socket.emit("message_received",loan_module);
    }else if (message === 'head branch') {
      socket.emit('message_received', head_branch); 
    }
    else {
      getIntentRequest(message, socket, userID);
    }
  });
});

// function checkInterventionExist(userId, redis) {
//   const client = redis.createClient({
//     host: process.env.REDIS_SERVER
//   });
//   client.on("error", function (err) {
//     console.log("Error " + err);
//   });
//   let id = userId + "_Agent";//replace a with A
//   return new Promise((resolve, reject) => {
//     client.exists(id, (err, reply) => {
//       resolve(reply);
//     });
//   });
// }

function postFbMessage(message, userID, type, senderId) {
  let host = process.env.MESSANGER_BOT_HOST;
  let port = process.env.MESSANGER_BOT_PORT;
  let chatKey = process.env.MESSANGER_BOT_KEY;
  let chatOrg = process.env.MESSANGER_BOT_ORG_ID;
  let protocol = "http://";
  let path = "/chathook";
  let recipientId = userID;
  let url = protocol + host + ":" + port + path;
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      chatkey: `sha1=${chatKey}`,
      organizationId: chatOrg,
    },
    body: JSON.stringify({
      object: "page",
      entry: [
        {
          id: "100836117978139",
          time: Date.now(),
          messaging: [
            {
              sender: {
                id: userID,
              },
              recipient: {
                id: recipientId,
              },
              timestamp: Date.now(),
              chat: {
                text: message,
                senderId: senderId,
              },
            },
          ],
        },
      ],
    }),
  }).catch((err) => {
    console.log("cant post");
  });
}

function setIntervention(userId) {
  let host = process.env.INTENT_SERVER;
  let port = process.env.INTENT_SERVER_PORT;
  let utterURL = process.env.INTENT_UTTER_URL;
  let protocol = "http://";
  let url =
    protocol + host + ":" + port + "/" + utterURL + "/agent?userid=" + userId;
  (async () => {
    const response = await fetch(url);
  })();
}
function getIntentRequest(utter, socket, userID) {
  let host = process.env.INTENT_SERVER;
  let port = process.env.INTENT_SERVER_PORT;
  let utterURL = process.env.INTENT_UTTER_URL;
  // let protocol = process.env.SOCKET_PROTOCOL+"://";

  let url = `${process.env.SOCKET_PROTOCOL}://${host}:${port}/${utterURL}/?q=${utter}`;
  console.log(url);
  fetch(url, {
    method: "GET",
  })
    .then((res) => res.json())
    .then((data) => {
      socket.emit("message_received", data.response);
      console.log("response data", data.response);
      let message = unhandleMSG;
      try {
        if (data.response) {
          if (data.response.hasOwnProperty("message")) {
            message = data.response;
          }
          if (data.response.title && typeof data.response.title === "string") {
            message = data.response.title;
          } else {
            if (
              typeof data.response.title === "object" &&
              Object.keys(data.response.title).length
            ) {
              if (data.response.title.hasOwnProperty("title")) {
                message = data.response.title.title;
              } else {
                message =
                  data.response.title[Object.keys(data.response.title)[0]];
              }
              if (typeof message === "object" && Object.keys(message).length) {
                message = message[Object.keys(message)[0]];
              }
            }
          }
        }
      } catch (err) {
        console.log("not captured data ");
      }

      return postMessage(message, userID, "outgoing", "bot");
    })
    .then((res) => res.json())
    .then((data) => {})
    .catch((error) => {
      // console.log("error ", error);
    });
}

//copy postmessage
function postMessage(message, userID, type, senderId) {
  let host = process.env.DASHBOARD_SERVER;
  let port = process.env.DASHBOARD_PORT;
  let protocol = "http://";
  let path =
    "/rest/v1/messages/userId/{id}?userId=" +
    userID +
    "&access_token=" +
    process.env.ADMIN_TOKEN;
  let url = protocol + host + ":" + port + path;
  if (typeof message === "object") {
    let title = "";
    for (key in message) {
      title = title + key + ":" + message[key] + " ";
    }
    message = title;
  }

  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      senderId: senderId,
      usertype: type == "incoming" ? "human" : "bot",
      text: message,
    }),
  })
    .then((data) => {
      // console.log("Posting Data",data);
    })
    .catch("Not posted");
}

module.exports = {
  app: app,
  server: server,
};
