const fetch = require("node-fetch");
//const url = `${process.env.baseImeUrl}`;
const JSONDATA=require('../localDataJson/train.json');
var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');
//base path url server
let protocal=process.env.SOCKET_PROTOCOL;
let host=process.env.IMEHost;
let port=process.env.IMEPORT;
let baseURL=process.env.IMEbaseURL;
let IMElastUrl=process.env.IMElastUrl;

//Agent policy
const url=`${protocal}://${host}:${port}${baseURL}`;
const loginAgent=`${url}TAgents/login${IMElastUrl}`;
const agentPolicy=`${url}TAgents/AgentPolicy${IMElastUrl}`;
const AgentInfo=`${url}TAgents/AgentInfo${IMElastUrl}`;
const AgentCommission=`${url}TAgents/AgentComToPay${IMElastUrl}`;

//Policy
const loginPolicy=`${url}TPolicies/login${IMElastUrl}`;
const getPolicyInfo=`${url}TPolicies/PolicyInfo${IMElastUrl}`;
const PolicyDueURL=`${url}TPolicies/PolDueInfo${IMElastUrl}`;
const PolicyStatementURL = `${url}TPolicies/PolicyStmnt${IMElastUrl}`;
const ln_policy=`${url}tPHL/GetPolLoanBal${IMElastUrl}`;

//New update  api URL for both Policy and Agent
const taxcerthificateURL=`${url}TPolicies/PolicyTaxInfo${IMElastUrl}`;
const checkpolictstatusURL=`${url}TPolicies/PolicyStatus${IMElastUrl}`;
const agentDownlineBusinessURL=`${url}TAgents/AgDownLineBusiness${IMElastUrl}`;

var base64 = require('base-64');

//Basic Authentication
let basicAuth=`${process.env.IMEusername}:${process.env.IMEpassword}`;

exports.postLeads = function(req, res, next) {
              console.log(req.body,"requested formsection")
  let { type, action, emailfor = 'something' } = req.query;
  this.$causeOfDeath=req.body.want;
  this.$email_OF_Death_Claimer=req.body.email;
  this.AssuredName = req.body.AssuredName;
  this.policynumber = req.body.PolicyNumber;
  this.Tentative_Loan_Amount = req.body.Tentative_Loan_Amount;
  this.sumassured = req.body.SumAssured;
              if(action=='email'){
                   sendEmail(type,emailfor); 
              }
              else if(action=='view'){
                  view(type,req,res);
              }
              else if(action=='post'){
                   modeType(type,req,res);
              }
              else{
                return false
              }

            function view(type,req,res){
              modeType(type,req,res)
            }


          //send email with diff catagories
            function sendEmail(type,emailFor){
                switch(emailFor){
                    //for Policy
                    case "ModeChange":
                        mailid=process.env.ModeChange;
                        title=process.env.ModeChangeTitle;
                        modechange=req.body.ModeType;
                        MailtoPolicyfetch(mailid,emailFor,title,modechange);
                        break 

                    // case "Revival":
                    //     mailid=process.env.Revival;
                    //     title=process.env.RevivalTitle;
                    //     MailtoPolicyfetch(mailid,emailFor,title,"Revival");
                    //     break

                    case "NomineeChange":
                        mailid=process.env.Nominee;
                        title=process.env.NomineeChangeTitle;
                        MailtoPolicyfetch(mailid,emailFor,title,"NomineeChange");
                        break

                  case "Occupation":
                    console.log('occupation change test')
                      mailid=process.env.occupationId;
                      title=process.env.occupationTitle;
                      MailtoPolicyfetch(mailid,emailFor,title,"Occupation");
                      break 

                    case "TAXCertificate":
                        mailid=process.env.TAXCertificate;
                        title=process.env.TAXCertificateTitle;
                        MailtoPolicyfetch(mailid,emailFor,title,"NomineeChange");
                        
                    case "MobileChange":
                        mailid=process.env.MobileChange;
                        title=process.env.MobileChangeTitle;
                        MailtoPolicyfetch(mailid,emailFor,title,"NomineeChange");
                        break
                    //for Agent
                    case "Updatepan":
                        mailid=process.env.Updatepin;
                        title=process.env.UpdatepinTitle;
                        MailtoAgentfetch(mailid,emailFor,title,"Updatepan");
                        break

                    case "UpdateBankName":
                        mailid=process.env.BankName;
                        title=process.env.BankNameTitle;
                        MailtoAgentfetch(mailid,emailFor,title,"BankName");
                        break 
                        
                    case "UpdateBankAcn":
                        mailid=process.env.UpdateBankAcn;
                        title=process.env.UpdateBankAcnTitle;
                        MailtoAgentfetch(mailid,emailFor,title,"UpdateBankAcn");
                        break
                    //general mail
                    case "BecomeAgent":
                        mailid=process.env.becomeAgent;
                        title="to bacome Agent";
                        data={
                          name:req.body.name,
                          location:req.body.address,
                          email:req.body.email,
                          mobile:req.body.mobile
                        }
                        submissionGeneralmail(mailid,emailFor,data,title,req,res);
                    break
                  //LOAN
                  case "LOAN":
                      console.log('name of loan need user', req.body.AssuredName);
                      MailtoLoanfetching();
                    break
                  
                  case "LoanMessage":
                       MailtoPolicyfetc(res);
                       break
                  //end of loan
                  //claim
                  case "new_claim":
                     this.$cause = this.$causeOfDeath;
                     this.$email_Death_claimer=this.$email_OF_Death_Claimer;
                     console.log('testing for new claim')
                    MailtoClaimFetch();
                    break
                  case "claim_Message":
                     console.log('send message regarding claim to ime office ')
                        MailtoClaimfetc(res);
                
                    break
                  //end of claim
                  //Revival
                  case "Revival":
                     MailtoPolicyfetchhh();                      
                break
                    default:
                        return
                } 
            }

            //send Mail to Agent holder
            function MailtoAgentfetch(mailid,mailfor,title,mode){
              let loginAgentURL=loginAgent;
              let AgentURL=agentPolicy;
              let postdata={
                AgentCode: req.body.AgentCode,
                LastName:req.body.LastName,
                DateOfBirth:req.body.DateOfBirth
                }
                fetch(loginAgentURL,{
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization':`Basic ${base64.encode(basicAuth)}`
                    },
                    body: JSON.stringify(postdata)
                  }).then(res =>res.json())
                    .then(data => {
                      if(+data[0]==0){
                        let agentcode=req.body.AgentCode;
                        fetchAgentInfoEmail(AgentURL,agentcode,mailid,mailfor,title,mode,req,res);
                      }
                      else{
                        res.status(200).json({
                            data:'failed to proceed',
                              type:"Agent"
                            })
                      }
                    }).catch(err => {
                      return false
                    });
            }
            
            //fetching agent information for to send with respective email
            function  fetchAgentInfoEmail(AgentURL,agentcode,mailid,mailfor,title,mode,req,res){
              let AgentCode={
                AgentCode:agentcode
              }
              fetch(AgentURL,{
                method: "POST",
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':`Basic ${base64.encode(basicAuth)}`
                },
                body: JSON.stringify(AgentCode)
              }).then(res =>res.json())
                .then(data => {
                  submissionmailAgency(mailid,mailfor,data,title,mode,req,res);
                })
                .catch(err=>console.log(err))
            }
           
            //Main Email functionality for Agency Email
            function submissionmailAgency(mailid,mailfor,data,title,mode,req,res){
              // console.log(data,title,mailid,mailfor,data,title,mode,req.headers.auth)
              let transporter = nodemailer.createTransport({
                  host: process.env.SMTP_HOST,
                  secure: false,
                  port: process.env.SMTP_PORT,
                auth: {
                      user:process.env.EMAIL ,
                      pass: process.env.PASSWORD
                  }
              });
              transporter.use('compile', hbs({
                viewEngine:"express-handlebars",
                viewPath:"./"
              }));
              let HelperOptions = {
                  from:process.env.EMAIL,
                  to:mailid,
                  subject:`${title}-${data[0].Name}(${data[0].Code})`,
                  template:"main",
                  context: {
                    agency:true,
                    subject:`${data[0].Name} with Agent no ${data[0].Code} want to ${title}`,
                    name:data[0].Name,
                    AgentCode:data[0].Code,
                    ComBranch:data[0].ComBranch,
                    Gender:data[0].Gender,
                    catagories:title
                  }
                };

                transporter.sendMail(HelperOptions, (error, info) => {            
                    if (error) {
                      //  console.log("mailconfig",error)
                    }
                    else {
                      res.status(200).json(HelperOptions);
                    }
                })       
                  

            }
         //LOAN
   function   MailtoLoanfetching(){
              let loginPolicyURL=loginPolicy;
              let PolicyURL=PolicyDueURL;
              let postdata={
                PolicyNumber:req.body.PolicyNumber,
                LastName:req.body.LastName,
                DOBAssured:req.body.DateOfBirth
                }
                fetch(loginPolicyURL,{
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization':`Basic ${base64.encode(basicAuth)}`
                    },
                    body: JSON.stringify(postdata)
                  }).then(res =>res.json())
                    .then(data => {
                      if(data[0]==0){
                        let agentcode=req.body.PolicyNumber;
                        console.log("vallid user");
                        console.log('login data',data)
                        MailtoPolicyfetcz(agentcode)
                      }
                      else{
                        res.status(200).json({
                            data:'failed to login',
                            type:"Policy"
                            })
                        console.log("invalid user")
                      }
                    }).catch(err => {
                      return false
                    });
   }
  function MailtoPolicyfetcz(agentcode) {
    //console.log('testing tesing 123')
    let loginPolicyURL = loginPolicy;
    let PolicyURL = PolicyDueURL;
    let LN_URL=ln_policy;
    let postdata = {
      PolicyNumber: agentcode,
      //LastName:req.body.LastName,
      //DOBAssured:req.body.DateOfBirth
    }
    console.log('postdata is',postdata)
    fetch(LN_URL, {
      method: "POST",
      headers: {
        //Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${base64.encode(basicAuth)}`
      },
      body: JSON.stringify(postdata)
    })
      .then(async response => await response.json())
      .then(data => {
        console.log(data)
        res.status(200).json({
          data: data,
          context: {
            catagories: "loan purpose"
          }
        })
      })
      .catch(err => console.log(err))
  }
    //loan message sender
  function MailtoPolicyfetc(res) {
              mailid=process.env.loan
    //console.log('joseph thebe hai')
    //console.log('mail id', mailid)
    this.$name = this.AssuredName;
    this.$policy = this.policynumber;
    this.$tent = this.Tentative_Loan_Amount;
    this.$sum = this.sumassured;
              let transporter = nodemailer.createTransport({
                  host: process.env.SMTP_HOST,
                  secure: false,
                  port: process.env.SMTP_PORT,
                auth: {
                      user:process.env.EMAIL ,
                      pass: process.env.PASSWORD
                  }
              });
              transporter.use('compile', hbs({
                viewEngine:"express-handlebars",
                viewPath:"./"
              }));
              let HelperOptions = {
                  from:process.env.EMAIL,
                  to:mailid,
                  subject:`Application for loan.`,
                  template:"main",
                  //context: {
                    // subject: `${this.AssuredName} wants to apply for loan `,
                    // Tentativeloanamount: this.Tentative_Loan_Amount,
                    // joseph:this.AssuredName

                    
                    //changeMobile:req.body.mobile===undefined?"":req.body.mobile,
                    //mobile:data[0].Mobile,
                    // PolicyNumber:data[0].PolicyNumber,
                    //PayMode:data[0].PayMode,
                    //PlanName:data[0].PlanName,
                    //AgentCode:data[0].AgentCode,
                    //AgentName:data[0].AgentName,
                    //catagories:title+ `${title==="change premium mode"?`- ${mode}.`:` `}`
                //}
                html:
                  ` <div style=" background-color: #eeeeee;padding:10px 10px 10px 10px;">
      <div style=" margin:10px 20px 10px 20px;background-color: #ffffff; padding: 0 20px 0 20px;">                
      <h4 style="margin-top: -18px;"> ${this.$name} wants to apply for loan. </h4>
      <br/>
      <p style="font-style: 800"> ${this.$name} with policy number ${this.$policy} wants to apply for loan. The details are :</p>
      <table>
        <tr>
      <td style="font-weight: 800;">Name</th>
      <td style="font-weight: 800;">${this.$name}</th>
    </tr>
    <tr>
      <td >Policy Number</td>
      <td>${this.$policy}</td>
    </tr >
    <tr>
      <td >Tentative loan Amount</td>
      <td>${this.$tent}</td>
    </tr >
   
    <tr>
      <td >Sum Assured</td>
      <td>${this.$sum}</td>
    </tr >
  <tr>
    <td>Subject</td>
    <td>Wants to apply for loan.</td>
  </tr>

  </table>
  <br/>
    </div >
    </div > `,
                 context: {
        
        name: this.AssuredName
        
      }
    
     };
     console.log('helperoptions are:', HelperOptions);
        
                transporter.sendMail(HelperOptions, (error, info) => {            
                    if (error) {
                      console.log("mailconfig",error)
                    }
                    else {
                      console.log('message sent successfully');
                      res.status(200).json(HelperOptions);
                      
                    }
                })       
          }
  
  //end of loan

  //claim
  //for Claim (recent new update)
   function MailtoClaimFetch(){
              this.$causes = this.$cause;
              this.$claimer_email=this.$email_Death_claimer;
              let loginPolicyURL=loginPolicy;
              let PolicyURL=PolicyDueURL;
              let postdata={
                PolicyNumber:req.body.PolicyNumber,
                LastName:req.body.LastName,
                DOBAssured:req.body.DateOfBirth
                }
                fetch(loginPolicyURL,{
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization':`Basic ${base64.encode(basicAuth)}`
                    },
                    body: JSON.stringify(postdata)
                  }).then(res =>res.json())
                    .then(data => {
                      console.log('response on login',data)
                      if(data[0]==0){
                        let agentcode=req.body.PolicyNumber;
                        console.log("vallid user");
                        MailtoClaim(agentcode);
                        
                      }
                      else{
                        res.status(200).json({
                            data:'failed to login',
                            type:"Policy"
                            })
                        console.log("invalid user")
                      }
                    }).catch(err => {
                      return false
                    });
            }

  function MailtoClaim(agentcode){
    console.log('testing tesing 123')
    let loginPolicyURL = loginPolicy;
    let PolicyURL = PolicyDueURL;
    let Access_URL=ln_policy;
    let postdata = {
      PolicyNumber: agentcode,
      //LastName:req.body.LastName,
      //DOBAssured:req.body.DateOfBirth
    }
    console.log('postdata is',postdata)
    fetch(ln_policy, {
      method: "POST",
      headers: {
        //Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${base64.encode(basicAuth)}`
      },
      body: JSON.stringify(postdata)
    })
      .then(async response => await response.json())
      .then(data => {
        console.log('data',data)
        console.log('full name',data[0].AssuredName)
        res.status(200).json({
          context: {
            catagories: "claim purpose",
            fullName:data[0].AssuredName,
            PolicyNumber:data[0]. policynumber,
            for:'new_claim'
          }
})
      })
      .catch(err => console.log(err))
  }


  //mail for claim
    this.$caused= this.$causes;
    this.$mailid_of_Claimer=this.$claimer_email;
  function  MailtoClaimfetc(res) {
  mailid=process.env.claim
    console.log('claim_message sending section')
    console.log('mail id', mailid)
    this.$name = this.AssuredName;
    this.$policy = this.policynumber;
    
              let transporter = nodemailer.createTransport({
                  host: process.env.SMTP_HOST,
                  secure: false,
                  port: process.env.SMTP_PORT,
                auth: {
                      user:process.env.EMAIL ,
                      pass: process.env.PASSWORD
                  }
              });
              transporter.use('compile', hbs({
                viewEngine:"express-handlebars",
                viewPath:"./"
              }));
              let HelperOptions = {
                  from:process.env.EMAIL,
                  to:mailid,
                  subject:`Application for Death Claim.`,
                  template:"main",
                  //context: {
                    // subject: `${this.AssuredName} wants to apply for loan `,
                    // Tentativeloanamount: this.Tentative_Loan_Amount,
                    // joseph:this.AssuredName

                    
                    //changeMobile:req.body.mobile===undefined?"":req.body.mobile,
                    //mobile:data[0].Mobile,
                    // PolicyNumber:data[0].PolicyNumber,
                    //PayMode:data[0].PayMode,
                    //PlanName:data[0].PlanName,
                    //AgentCode:data[0].AgentCode,
                    //AgentName:data[0].AgentName,
                    //catagories:title+ `${title==="change premium mode"?`- ${mode}.`:` `}`
                //}
                html:
                  ` <div style=" background-color: #eeeeee;padding:10px 10px 10px 10px;">
      <div style=" margin:10px 20px 10px 20px;background-color: #ffffff; padding: 0 20px 0 20px;">                
      <h4 style="margin-top: -18px;"> ${this.$name} wants to apply for Death Claim. </h4>
      <br/>
      <p style="font-style: 800"> ${this.$name} with policy number ${this.$policy} wants to register a Death claim. The details are :</p>
      <table>
        <tr>
      <td style="font-weight: 800;">Name</th>
      <td style="font-weight: 800;">${this.$name}</th>
    </tr>
    <tr>
      <td >Policy Number</td>
      <td>${this.$policy}</td>
    </tr >
     <tr>
      <td ">Cause of Death</th>
      <td ">${this.$caused}</th>
    </tr>
   
    
  

  </table>
   `,
          context: {
        
        name: this.AssuredName
        
      }
    
     };
     console.log('helperoptions are:', HelperOptions);
        
                transporter.sendMail(HelperOptions, (error, info) => {            
                    if (error) {
                      console.log("mailconfig",error)
                    }
                    else {
                      console.log('message sent successfully');
                      res.status(200).json(HelperOptions);
                      
                    }
                })  
  documentSenderForClaim(this.$name,this.$policy);  

          }
  
  
  
  
    //for sending required documents to client regarding Death claim
    function documentSenderForClaim(name,policy) {
  var nodemailer = require('nodemailer');
      mailid = process.env.Revival;
      claim_url = process.env.claim_path;
    let transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      secure: false,
      port: process.env.SMTP_PORT,
      auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
      }
    });
    transporter.use('compile', hbs({
      viewEngine: "express-handlebars",
      viewPath: "./"
    }));
    let HelperOptions = {
      from: process.env.EMAIL,
      to: this.$mailid_of_Claimer ,
      subject: `Dear ${name} , these are the forms to be sumbitted for Death Claim. Please fill up the form and send it to ime life offical email . `,
      template: "main",
       attachments: [{
                        filename: 'Dabikartale Statement',
                        path: `${claim_url}6.Dabikartale Bharne statement.pdf`,
                        contentType: 'application/pdf'
                    },
                  {
                        filename: 'Parichaya Praman Patra filled by other person than family.pdf',
                        path: `${claim_url}7.Parichaya Praman Patra_filled by other person than family.pdf`,
                        contentType: 'application/pdf'
                    },
                    {
                        filename: 'Agent statement by Agent .pdf',
                        path: `${claim_url}8.Agent statement by Agent .pdf`,
                        contentType: 'application/pdf'
                    },
                    {
                        filename: 'Certificate of Employer.pdf',
                        path: `${claim_url}Certificate of Employer.pdf`,
                        contentType: 'application/pdf'
                    },{
                        filename: 'Certificate of hospital Treatment.pdf',
                        path: `${claim_url}Certificate of hospital Treatment.pdf`,
                        contentType: 'application/pdf'
                    },{
                        filename: 'Discharge voucher _After amount Finilize.pdf',
                        path: `${claim_url}Discharge voucher _After amount Finilize.pdf`,
                        contentType: 'application/pdf'
                    },
                  {
                        filename: 'IME KYC form.pdf',
                        path: `${claim_url}IME KYC form.pdf`,
                        contentType: 'application/pdf'
                    },
                     {
                        filename: 'Medical Attendance Certificate.pdf',
                        path: `${claim_url}Medical Attendance Certificate.pdf`,
                        contentType: 'application/pdf'
                    }
                  
                  
                  
                  ]
    
    };
    console.log('helperoptions are:', HelperOptions);
        
    transporter.sendMail(HelperOptions, (error, info) => {
      if (error) {
        console.log("mailconfig", error)
      }
      else {
        console.log('message sent successfully');
        res.status(200).json(HelperOptions);
                      
      }
    })
}



  
  
  
  
  //end of claim

//revival
  
   function MailtoPolicyfetchhh(){
              let loginPolicyURL=loginPolicy;
              let PolicyURL=PolicyDueURL;
              let postdata={
                PolicyNumber:req.body.PolicyNumber,
                LastName:req.body.LastName,
                DOBAssured:req.body.DateOfBirth
                }
                fetch(loginPolicyURL,{
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization':`Basic ${base64.encode(basicAuth)}`
                    },
                    body: JSON.stringify(postdata)
                  }).then(res =>res.json())
                    .then(data => {
                      if(data[0]==0){
                        let agentcode=req.body.PolicyNumber;
                        console.log("vallid user");
                         MailtoReviveFetch(agentcode)}
                      else{
                        res.status(200).json({
                            data:'failed to login',
                            type:"Policy"
                            })
                        console.log("invalid user")
                      }
                    }).catch(err => {
                      return false
                    });
            }


    function MailtoReviveFetch(agentcode){
    console.log('for revive data of use access')
    let loginPolicyURL = loginPolicy;
    let PolicyURL = PolicyDueURL;
    let new_Ln_URL=ln_policy;
    let postdata = {
      PolicyNumber: agentcode,
      //LastName:req.body.LastName,
      //DOBAssured:req.body.DateOfBirth
    }
    console.log('postdata is',postdata)
    fetch(new_Ln_URL, {
      method: "POST",
      headers: {
        //Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Basic ${base64.encode(basicAuth)}`
      },
      body: JSON.stringify(postdata)
    })
      .then(async response => await response.json())
      .then(data => {
        console.log('data',data)
        console.log('full name',data[0].AssuredName)
        res.status(200).json(
          {
                          // status: 200,
                          // Email:req.body.email,
                          // fullName:data[0].AssuredName,
                          // PolicyNumber:data[0]. policynumber,
            context: {
            catagories: "revival purpose",
            status: 200,
            Email:req.body.email,
            fullName:data[0].AssuredName,
             PolicyNumber:data[0]. policynumber,
            },
            for:"revival purpose"
                          
          }
        )
      })
      .catch(err => console.log(err))
  }
  
  //end of revival
  


            //Send Mail to Policy holder
            function MailtoPolicyfetch(mailid,mailfor,title,mode){
              let loginPolicyURL=loginPolicy;
              let PolicyURL = PolicyDueURL;
              console.log('MailtoPolicyfetch test');
              console.log('loginPolicyUR', loginPolicyURL);
              console.log('PolicyURL',PolicyURL );
              let postdata={
                PolicyNumber:req.body.PolicyNumber,
                LastName:req.body.LastName,
                DOBAssured:req.body.DateOfBirth
                }
                fetch(loginPolicyURL,{
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization':`Basic ${base64.encode(basicAuth)}`
                    },
                    body: JSON.stringify(postdata)
                  }).then(res =>res.json())
                    .then(data => {
                      if(+data[0]==0){
                        let agentcode=req.body.PolicyNumber;
                        console.log("vallid user");
                        fetchPolicyDuePolicyEmail(PolicyURL,agentcode,mailid,mailfor,title,mode,req,res);
                      }
                      else{
                        res.status(200).json({
                            data:'failed to proceed',
                              type:"Policy"
                            })
                        console.log("invalid user")
                      }
                    }).catch(err => {
                      return false
                    });
            }
            
            function fetchPolicyDuePolicyEmail(PolicyURL,agentcode,mailid,mailfor,title,mode,req,res){
              let AgentCode={
                PolicyNumber:agentcode
              }
              fetch(PolicyURL,{
                method: "POST",
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':`Basic ${base64.encode(basicAuth)}`
                },
                body: JSON.stringify(AgentCode)
              }).then(res =>res.json())
                .then(data => {
                if(mailfor==="MobileChange"){
                  MobileChangesubmission(mailid,mailfor,data,title,mode,req,res)
                }
                if(mailfor==="Occupation"){
                  OccupationChangesubmission(mailid,mailfor,data,title,mode,req,res)
                }
                else{
                  submission(mailid,mailfor,data,title,mode,req,res)
                }
               
                })
                .catch(err=>console.log(err))
            }
            //end mail fetch with different departments


             function OccupationChangesubmission(mailid,mailfor,data,title,mode,req,res){
              let transporter = nodemailer.createTransport({
                  host: process.env.SMTP_HOST,
                  secure: false,
                  port: process.env.SMTP_PORT,
                auth: {
                      user:process.env.EMAIL ,
                      pass: process.env.PASSWORD
                  }
              });
              transporter.use('compile', hbs({
                viewEngine:"express-handlebars",
                viewPath:"./"
              }));
              let HelperOptions = {
                  from:process.env.EMAIL,
                  to:mailid,
                  subject:`${title}-${data[0].AssuredName}(${data[0].PolicyNumber})`,
                  template:"main",
                  context: {
                    subject:`${data[0].AssuredName} with policy no ${data[0].PolicyNumber}  want to ${title} ${title==="change premium mode"?`to ${mode}.`:``} ${title==="Change Mobile number"?`(${req.body.mobile}).`:``}`,
                    name:data[0].AssuredName,
                    occupation:req.body.occupation===undefined?"":req.body.occupation,
                    mobile:data[0].Mobile,
                    // PolicyNumber:data[0].PolicyNumber,
                    PayMode:data[0].PayMode,
                    PlanName:data[0].PlanName,
                    AgentCode:data[0].AgentCode,
                    AgentName:data[0].AgentName,
                    catagories:title+ `${title==="change premium mode"?`- ${mode}.`:` `}`
                  }
                };
        
                transporter.sendMail(HelperOptions, (error, info) => {            
                    if (error) {
                      //  console.log("mailconfig",error)
                    }
                    else {
                      res.status(200).json(HelperOptions);
                      
                    }
                })       
          }

            function MobileChangesubmission(mailid,mailfor,data,title,mode,req,res){
              let transporter = nodemailer.createTransport({
                  host: process.env.SMTP_HOST,
                  secure: false,
                  port: process.env.SMTP_PORT,
                auth: {
                      user:process.env.EMAIL ,
                      pass: process.env.PASSWORD
                  }
              });
              transporter.use('compile', hbs({
                viewEngine:"express-handlebars",
                viewPath:"./"
              }));
              let HelperOptions = {
                  from:process.env.EMAIL,
                  to:mailid,
                  subject:`${title}-${data[0].AssuredName}(${data[0].PolicyNumber})`,
                  template:"main",
                  context: {
                    subject:`${data[0].AssuredName} with policy no ${data[0].PolicyNumber}  want to ${title} ${title==="change premium mode"?`to ${mode}.`:``} ${title==="Change Mobile number"?`(${req.body.mobile}).`:``}`,
                    name:data[0].AssuredName,
                    changeMobile:req.body.mobile===undefined?"":req.body.mobile,
                    mobile:data[0].Mobile,
                    // PolicyNumber:data[0].PolicyNumber,
                    PayMode:data[0].PayMode,
                    PlanName:data[0].PlanName,
                    AgentCode:data[0].AgentCode,
                    AgentName:data[0].AgentName,
                    catagories:title+ `${title==="change premium mode"?`- ${mode}.`:` `}`
                  }
                };
        
                transporter.sendMail(HelperOptions, (error, info) => {            
                    if (error) {
                      //  console.log("mailconfig",error)
                    }
                    else {
                      res.status(200).json(HelperOptions);
                      
                    }
                })       
          }

            function submission(mailid,mailfor,data,title,mode,req,res){
              console.log(data,"from mail")
                let transporter = nodemailer.createTransport({
                    host: process.env.SMTP_HOST,
                    secure: false,
                    port: process.env.SMTP_PORT,
                  auth: {
                        user:process.env.EMAIL ,
                        pass: process.env.PASSWORD
                    }
                });
                transporter.use('compile', hbs({
                  viewEngine:"express-handlebars",
                  viewPath:"./"
                }));
                let HelperOptions = {
                    from:process.env.EMAIL,
                    to:mailid,
                    subject:`${title}-${data[0].AssuredName}(${data[0].PolicyNumber})`,
                    template:"main",
                    context: {
                      subject:`${data[0].AssuredName} with policy no ${data[0].PolicyNumber}  want to ${title} ${title==="change premium mode"?`to ${mode}.`:``} ${title==="Change Mobile number"?`(${req.body.mobile}).`:``}`,
                      name:data[0].AssuredName,
                      // changeMobile:req.body.mobile===undefined?"":req.body.mobile,
                      mobile:data[0].Mobile,
                      PolicyNumber:data[0].PolicyNumber,
                      PayMode:data[0].PayMode,
                      PlanName:data[0].PlanName,
                      AgentCode:data[0].AgentCode,
                      AgentName:data[0].AgentName,
                      catagories:title+ `${title==="change premium mode"?`- ${mode}.`:` `}`
                    }
                  };
          
                  transporter.sendMail(HelperOptions, (error, info) => {            
                      if (error) {
                         console.log("mailconfig",error)
                      }
                      else {
                        console.log("bikash")
                        res.status(200).json(HelperOptions);
                        
                      }
                  })       
            }


            function submissionGeneralmail(mailid,mailfor,data,title,req,res){
              let transporter = nodemailer.createTransport({
                  host: process.env.SMTP_HOST,
                  secure: false,
                  port: process.env.SMTP_PORT,
                auth: {
                      user:process.env.EMAIL ,
                      pass: process.env.PASSWORD
                  }
              });
              transporter.use('compile', hbs({
                viewEngine:"express-handlebars",
                viewPath:"./"
              }));
              let HelperOptions = {
                  from:process.env.EMAIL,
                  to:mailid,
                  subject:`${title}-${data.name}`,
                  template:"main",
                  context: {
                    subject:data.name+ ` want to become Agent.`,
                    general:true,
                    name:data.name,
                    mobile:data.mobile,
                    location:data.location,
                    email:data.email,
                    catagories:title
                  }
                };

                transporter.sendMail(HelperOptions, (error, info) => {            
                    if (error) {
                      //  console.log("mailconfig",error)
                    }
                    else {
                      res.status(200).json(HelperOptions);
                      console.log("submited")
                    }
                })       
                  

          }


            function modeType(type,req,res){
                switch(type){
                    case "policy_dueinfo":
                        Policyfetch(loginPolicy,PolicyDueURL,"Policy Due Info",req,res);
                        break;

                    case "policy_statement":
                        Policyfetch(loginPolicy,PolicyStatementURL,"Your Statement",req,res);
                        break;

                        //update api for tax certhificate print
                    case "tax_certhificate":
                        Policyfetch(loginPolicy,taxcerthificateURL,"Tax Certificate",req,res);
                        break;    
                        
                          //update api for Policy Status
                    case "check_policy_status":
                      Policyfetch(loginPolicy,checkpolictstatusURL,"Policy Status",req,res);
                      break; 

                    case "AgentInfo":
                          Agentfetch(loginAgent,AgentInfo,"Agent Information",req,res);
                          break;

                    case "AgentPolicy":
                          Agentfetch(loginAgent,agentPolicy,"Issued Policies",req,res);
                          break;  
                          
                    case "AgentComToPay":
                          Agentfetch(loginAgent,AgentCommission,"Your Commission",req,res);
                          break;
                          
                    case "AgentDuedate":
                            Agentfetch(loginAgent,agentPolicy,"Due Policies",req,res);
                            break;  

                            //update api for agent downline Business
                    case "AgentDownlineBusiness":
                              Agentfetch(loginAgent,agentDownlineBusinessURL,"Agent Downline Business",req,res);
                              break;  
                    

                    default:
                      return;        

                }
            }

          
            function Policyfetch(loginPolicy,PolicyURL,title,req,res){
              console.log("Policy fetch api");
              let postdata={
                PolicyNumber:req.body.PolicyNumber,
                LastName:req.body.LastName,
                DOBAssured:req.body.DateOfBirth
                }
                fetch(loginPolicy,{
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization':`Basic ${base64.encode(basicAuth)}`
                    },
                    body: JSON.stringify(postdata)
                  }).then(res =>res.json())
                    .then(data => {
                      // console.log('posted', data,'and',data[0]);
                      if(+data[0]==0){
                        let agentcode=req.body.PolicyNumber;
                        // console.log("vallid user")
                        if(type==='policy_dueinfo'){
                              fetchPolicyDuePolicy(PolicyURL,agentcode,title,req,res);
                        }
                      else{
                              console.log("login in successfully")
                              fetchPolicy(PolicyURL,agentcode,title,req,res);
                        }              }
                      else{
                        res.status(200).json({
                            data:'failed to login',
                              type:"Policy"
                            })
                      }
                    }).catch(err => {
                      return false
                    });
            }

            function  Agentfetch(loginAgent,agentPolicy,title,req,res){
              let postdata={
                AgentCode: req.body.AgentCode,
                LastName:req.body.LastName,
                DateOfBirth:req.body.DateOfBirth
                }
                fetch(loginAgent,{
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json',
                      'Authorization':`Basic ${base64.encode(basicAuth)}`
                    },
                    body: JSON.stringify(postdata)
                  }).then(res =>res.json())
                    .then(data => {
                      // console.log('posted', data,'and',data[0]);
                      if(+data[0]==0){
                        let agentcode=req.body.AgentCode;
                        //  console.log("vallid user")
                        if(type==='AgentDuedate'){
                          fetchAgentDuePolicy(agentPolicy,agentcode,title,req,res);
                        }
                        if(type==="AgentDownlineBusiness"){
                          fetchAgentDownlineBusiness(agentPolicy,agentcode,title,req,res);
                        }
                        else{
                          fetchAgentPolicy(agentPolicy,agentcode,title,req,res);
                        }
                      }
                      else{
                        res.status(200).json({
                            data:'failed to login',
                            success:false,
                              type:"Agent"
                            })
                        // console.log("invalid user")
                      }
                    }).catch(err => {

                      return false
                      //for testing purposed
                      // const testdata=[{"Type":"PolicyHolderLoan","fiscalyear":"7677","policynumber":"503000121","sanctionamt":90000.0000,"status":"NEW"}];
                      // res.status(200).json({
                      //   type:type,
                      //   subtitle:title,
                      //   for:type,
                      //   data:testdata
                      // })


                    });
            }

            //for  fetch Agent Downline Business
            function  fetchAgentDownlineBusiness(url,agentcode,title,req,res){
              let postdata={
                AgentCode:agentcode,
                FromDate:req.body.FromDate,
                ToDate:req.body.ToDate,
                Downline:req.body.Downline
              }
              fetch(url,{
                method: "POST",
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':`Basic ${base64.encode(basicAuth)}`
                },
                body: JSON.stringify(postdata)
              }).then(res =>res.json())
                .then(data => {
                  console.log(data,"datas downline")
                  res.status(200).json({
                    type:type,
                    subtitle:title,
                    for:type,
                    date:{ 
                       FromDate:req.body.FromDate,
                      ToDate:req.body.ToDate},
                    data:data
                  })
                })
                .catch(err=>console.log(err))
            }

            function fetchAgentPolicy(url,agentcode,title,req,res){
              let AgentCode={
                AgentCode:agentcode
              }
              fetch(url,{
                method: "POST",
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':`Basic ${base64.encode(basicAuth)}`
                },
                body: JSON.stringify(AgentCode)
              }).then(res =>res.json())
                .then(data => {
                  res.status(200).json({
                    type:type,
                    subtitle:title,
                    for:type,
                    data:data
                  })
                })
                .catch(err=>console.log(err))
              }


            function fetchPolicy(url,agentcode,title,req,res){
                let policycode={
                  PolicyNumber:agentcode
                }
                fetch(url,{
                  method: "POST",
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Basic ${base64.encode(basicAuth)}`
                  },
                  body: JSON.stringify(policycode)
                }).then(res =>res.json())
                  .then(data => {
                    console.log(data,"Policy api")
                    res.status(200).json({
                      type:type,
                      subtitle:title,
                      code:agentcode,
                      for:type,
                      data:data
                    })
                  })
                  .catch(err=>console.log(err))
              }

            // fetching policy with response type filterable data
              function fetchPolicyDuePolicy(url,agentcode,title,req,res){
                let AgentCode={
                  PolicyNumber:agentcode
                }
                fetch(url,{
                  method: "POST",
                  headers: {
                    'Content-Type': 'application/json',
                    'Authorization':`Basic ${base64.encode(basicAuth)}`
                  },
                  body: JSON.stringify(AgentCode)
                }).then(res =>res.json())
                  .then(data => {
                    res.status(200).json({
                      type:type,
                      subtitle:title,
                      for:type,
                      data:data
                    })
                  })
                  .catch(err=>console.log(err))
              }
        
        // fetching Agent with response type filterable data
            function fetchAgentDuePolicy(url,agentcode,title,req,res){
              let AgentCode={
                AgentCode:agentcode
              }
              fetch(url,{
                method: "POST",
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization':`Basic ${base64.encode(basicAuth)}`
                },
                body: JSON.stringify(AgentCode)
              }).then(res =>res.json())
                .then(data => {
                  res.status(200).json({
                    type:type,
                    subtitle:title,
                    for:'policy_due',
                    data:data
                  })
                })
                .catch(err=>console.log(err))
            }
  

};
