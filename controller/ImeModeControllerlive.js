const fetch = require("node-fetch");
//const url = `${process.env.baseImeUrl}`;
const JSONDATA=require('../localDataJson/train.json');
var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');

let protocal=process.env.SOCKET_PROTOCOL;
let host=process.env.IMEHost;
let port=process.env.IMEPORT;
let baseURL=process.env.IMEbaseURL;
let IMElastUrl=process.env.IMElastUrl;
//Agent policy
const url=`${protocal}://${host}:${port}${baseURL}`;
const loginAgent=`${url}TAgents/login${IMElastUrl}`;
const agentPolicy=`${url}TAgents/AgentPolicy${IMElastUrl}`;
const AgentInfo=`${url}TAgents/AgentInfo${IMElastUrl}`;
const AgentCommission=`${url}TAgents/AgentComToPay${IMElastUrl}`;
//Policy
const loginPolicy=`${url}TPolicies/login${IMElastUrl}`;
const getPolicyInfo=`${url}TPolicies/PolicyInfo${IMElastUrl}`;
const PolicyDueURL=`${url}TPolicies/PolDueInfo${IMElastUrl}`;
const PolicyStatementURL=`${url}TPolicies/PolicyStmnt${IMElastUrl}`;

var base64 = require('base-64');
let basicAuth=`${process.env.IMEusername}:${process.env.IMEpassword}`;

exports.postLeads = function(req, res, next) {
    let {type,action,emailfor='something'} = req.query;

    if(action=='email'){
       sendEmail(type,emailfor); 
    }
    else if(action=='view'){
         view(type,req,res);
    }
    else if(action=='post'){
      modeType(type,req,res);
    }
    else{
      return false
    }

    function view(type,req,res){
      modeType(type,req,res)
    }



    function sendEmail(type,emailFor){
        switch(emailFor){
            //for Policy
            case "ModeChange":
                mailid=process.env.ModeChange;
                title=process.env.ModeChangeTitle;
                modechange=req.body.ModeType;
                MailtoPolicyfetch(mailid,emailFor,title,modechange);
                break 

            case "Revival":
                mailid=process.env.Revival;
                title=process.env.RevivalTitle;
                MailtoPolicyfetch(mailid,emailFor,title,"Revival");
                break

            case "NomineeChange":
                mailid=process.env.Nominee;
                title=process.env.NomineeChangeTitle;
                MailtoPolicyfetch(mailid,emailFor,title,"NomineeChange");
                break

            case "Occupation":
              mailid=process.env.occupationId;
              title=process.env.occupationTitle;
              MailtoPolicyfetch(mailid,emailFor,title,"Occupation");
              break 

            case "TAXCertificate":
                mailid=process.env.TAXCertificate;
                title=process.env.TAXCertificateTitle;
                MailtoPolicyfetch(mailid,emailFor,title,"NomineeChange");
                
            case "MobileChange":
                mailid=process.env.MobileChange;
                title=process.env.MobileChangeTitle;
                MailtoPolicyfetch(mailid,emailFor,title,"NomineeChange");
                break
            //for Agent
            case "Updatepin":
                mailid=process.env.Updatepin;
                title=process.env.UpdatepinTitle;
                MailtoAgentfetch(mailid,emailFor,title,"Updatepan");
                break

            case "UpdateBankName":
                mailid=process.env.BankName;
                title=process.env.BankNameTitle;
                MailtoAgentfetch(mailid,emailFor,title,"BankName");
                break 
                
            case "UpdateBankAcn":
                mailid=process.env.UpdateBankAcn;
                title=process.env.UpdateBankAcnTitle;
                MailtoAgentfetch(mailid,emailFor,title,"UpdateBankAcn");
                break
            //general mail
            case "BecomeAgent":
                mailid=process.env.becomeAgent;
                title="to bacome Agent";
                data={
                  name:req.body.name,
                  location:req.body.address,
                  email:req.body.email,
                  mobile:req.body.mobile
                }
                submissionGeneralmail(mailid,emailFor,data,title,req,res);
                break
                
                

            default:
                return
        } 
    }

    //send Mail to Agent holder
    function MailtoAgentfetch(mailid,mailfor,title,mode){
      let loginAgentURL=loginAgent;
      let AgentURL=agentPolicy;
      let postdata={
        AgentCode: req.body.AgentCode,
        LastName:req.body.LastName,
        DateOfBirth:req.body.DateOfBirth
        }
        fetch(loginAgentURL,{
            method: "POST",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':`Basic ${base64.encode(basicAuth)}`
            },
            body: JSON.stringify(postdata)
          }).then(res =>res.json())
            .then(data => {
              if(+data[0]==0){
                let agentcode=req.body.AgentCode;
                fetchAgentInfoEmail(AgentURL,agentcode,mailid,mailfor,title,mode,req,res);
              }
              else{
                 res.status(200).json({
                     data:'failed to proceed',
                      type:"Agent"
                    })
              }
            }).catch(err => {
              return false
            });
    }

    function  fetchAgentInfoEmail(AgentURL,agentcode,mailid,mailfor,title,mode,req,res){
      let AgentCode={
        AgentCode:agentcode
      }
      fetch(AgentURL,{
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization':`Basic ${base64.encode(basicAuth)}`
        },
        body: JSON.stringify(AgentCode)
      }).then(res =>res.json())
        .then(data => {
          submissionmailAgency(mailid,mailfor,data,title,mode,req,res);
        })
        .catch(err=>console.log(err))
    }

    function submissionmailAgency(mailid,mailfor,data,title,mode,req,res){
      // console.log(data,title,mailid,mailfor,data,title,mode,req.headers.auth)
      let transporter = nodemailer.createTransport({
          host: process.env.SMTP_HOST,
          secure: false,
          port: process.env.SMTP_PORT,
         auth: {
              user:process.env.EMAIL ,
              pass: process.env.PASSWORD
          }
      });
      transporter.use('compile', hbs({
        viewEngine:"express-handlebars",
        viewPath:"./"
      }));
      let HelperOptions = {
          from:process.env.EMAIL,
          to:mailid,
          subject:`${title}-${data[0].Name}(${data[0].Code})`,
          template:"main",
          context: {
            agency:true,
            subject:`${data[0].Name} with Agent no ${data[0].Code} want to ${title}`,
            name:data[0].Name,
            AgentCode:data[0].Code,
            ComBranch:data[0].ComBranch,
            Gender:data[0].Gender,
            catagories:title
           }
        };

         transporter.sendMail(HelperOptions, (error, info) => {            
            if (error) {
              //  console.log("mailconfig",error)
            }
            else {
              res.status(200).json(HelperOptions);
            }
        })       
          

  }




    //Send Mail to Policy holder
    function MailtoPolicyfetch(mailid,mailfor,title,mode){
      let loginPolicyURL=loginPolicy;
      let PolicyURL=PolicyDueURL;
      let postdata={
        PolicyNumber:req.body.PolicyNumber,
        LastName:req.body.LastName,
        DOBAssured:req.body.DateOfBirth
        }
        fetch(loginPolicyURL,{
            method: "POST",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':`Basic ${base64.encode(basicAuth)}`
            },
            body: JSON.stringify(postdata)
          }).then(res =>res.json())
            .then(data => {
              // console.log('posted', data,'and',data[0]);
              if(+data[0]==0){
                let agentcode=req.body.PolicyNumber;
                // console.log("vallid user");
                fetchPolicyDuePolicyEmail(PolicyURL,agentcode,mailid,mailfor,title,mode,req,res);
              }
              else{
                 res.status(200).json({
                     data:'failed to proceed',
                      type:"Policy"
                    })
                // console.log("invalid user")
              }
            }).catch(err => {
              // console.log('Capturing server not respond yet || Not captures Lead');
              return false
            });
    }
    
    function fetchPolicyDuePolicyEmail(PolicyURL,agentcode,mailid,mailfor,title,mode,req,res){
      let AgentCode={
        PolicyNumber:agentcode
      }
      fetch(PolicyURL,{
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization':`Basic ${base64.encode(basicAuth)}`
        },
        body: JSON.stringify(AgentCode)
      }).then(res =>res.json())
        .then(data => {
         submission(mailid,mailfor,data,title,mode,req,res)
        })
        .catch(err=>console.log(err))
    }
    //end mail fetch with different departments


    function submission(mailid,mailfor,data,title,mode,req,res){
        console.log(req.body);
        let transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
            secure: false,
            port: process.env.SMTP_PORT,
           auth: {
                user:process.env.EMAIL ,
                pass: process.env.PASSWORD
            }
        });
        transporter.use('compile', hbs({
          viewEngine:"express-handlebars",
          viewPath:"./"
        }));
        let HelperOptions = {
            from:process.env.EMAIL,
            to:mailid,
            subject:`${title}-${data[0].AssuredName}(${data[0].PolicyNumber})`,
            template:"main",
            context: {
              subject:`${data[0].AssuredName} with policy no ${data[0].PolicyNumber}  want to ${title} ${title==="change premium mode"?`to ${mode}.`:``} ${title==="Change Mobile number"?`(${req.body.mobile}).`:``}`,
              name:data[0].AssuredName,
              mobile:data[0].Mobile,
              PolicyNumber:data[0].PolicyNumber,
              PayMode:data[0].PayMode,
              PlanName:data[0].PlanName,
              AgentCode:data[0].AgentCode,
              AgentName:data[0].AgentName,
              catagories:title+ `${title==="change premium mode"?`- ${mode}.`:` `}`
             }
          };
  
           transporter.sendMail(HelperOptions, (error, info) => {            
              if (error) {
                //  console.log("mailconfig",error)
              }
              else {
                res.status(200).json(HelperOptions);
              }
          })       
    }


    function submissionGeneralmail(mailid,mailfor,data,title,req,res){
      let transporter = nodemailer.createTransport({
          host: process.env.SMTP_HOST,
          secure: false,
          port: process.env.SMTP_PORT,
         auth: {
              user:process.env.EMAIL ,
              pass: process.env.PASSWORD
          }
      });
      transporter.use('compile', hbs({
        viewEngine:"express-handlebars",
        viewPath:"./"
      }));
      let HelperOptions = {
          from:process.env.EMAIL,
          to:mailid,
          subject:`${title}-${data.name}`,
          template:"main",
          context: {
            subject:data.name+ ` want to become Agent.`,
            general:true,
            name:data.name,
            mobile:data.mobile,
            location:data.location,
            email:data.email,
            catagories:title
           }
        };

         transporter.sendMail(HelperOptions, (error, info) => {            
            if (error) {
              //  console.log("mailconfig",error)
            }
            else {
              res.status(200).json(HelperOptions);
            }
        })       
          

  }


    function modeType(type,req,res){
        switch(type){
            case "policy_dueinfo":
                Policyfetch(loginPolicy,PolicyDueURL,"Policy Due Info",req,res);
                break;

            case "policy_statement":
                Policyfetch(loginPolicy,PolicyStatementURL,"Your Statement",req,res);
                break;

            case "AgentInfo":
                  Agentfetch(loginAgent,AgentInfo,"Agent Information",req,res);
                  break;

            case "AgentPolicy":
                  Agentfetch(loginAgent,agentPolicy,"Issued Policies",req,res);
                  break;  
                  
            case "AgentComToPay":
                  Agentfetch(loginAgent,AgentCommission,"Your Commission",req,res);
                  break;
                  
            case "AgentDuedate":
                    Agentfetch(loginAgent,agentPolicy,"Due Policies",req,res);
                    break;      

            default:
               return;        

        }
    }

   
    function Policyfetch(loginPolicy,PolicyURL,title,req,res){
      let postdata={
        PolicyNumber:req.body.PolicyNumber,
        LastName:req.body.LastName,
        DOBAssured:req.body.DateOfBirth
        }
        fetch(loginPolicy,{
            method: "POST",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':`Basic ${base64.encode(basicAuth)}`
            },
            body: JSON.stringify(postdata)
          }).then(res =>res.json())
            .then(data => {
              console.log('posted', data,'and',data[0]);
              if(+data[0]==0){
                let agentcode=req.body.PolicyNumber;
                // console.log("vallid user")
                if(type==='policy_dueinfo'){
                 fetchPolicyDuePolicy(PolicyURL,agentcode,title,req,res);
                }
               else{
               fetchPolicy(PolicyURL,agentcode,title,req,res);
                }              }
              else{
                 res.status(200).json({
                     data:'failed to login',
                      type:"Policy"
                    })

                // console.log("invalid user")
              }
            }).catch(err => {
              // console.log('Capturing server not respond yet || Not captures Lead');
              return false
            });
    }

    function  Agentfetch(loginAgent,agentPolicy,title,req,res){
      let postdata={
        AgentCode: req.body.AgentCode,
        LastName:req.body.LastName,
        DateOfBirth:req.body.DateOfBirth
        }
        fetch(loginAgent,{
            method: "POST",
            headers: {
              'Content-Type': 'application/json',
              'Authorization':`Basic ${base64.encode(basicAuth)}`
            },
            body: JSON.stringify(postdata)
          }).then(res =>res.json())
            .then(data => {
              if(+data[0]==0){
                let agentcode=req.body.AgentCode;
                //  console.log("vallid user")
                 if(type==='AgentDuedate'){
                  fetchAgentDuePolicy(agentPolicy,agentcode,title,req,res);
                 }
                 else{
                  fetchAgentPolicy(agentPolicy,agentcode,title,req,res);
                 }
              }
              else{
                res.status(200).json({
                     data:'failed to login',
                     success:false,
                      type:"Agent"
                    })
                // console.log("invalid user")
              }
            }).catch(err => {
              // console.log('Capturing server not respond yet || Not captures Lead');
              return false
            });
    }


    function fetchAgentPolicy(url,agentcode,title,req,res){
      let AgentCode={
        AgentCode:agentcode
      }
      fetch(url,{
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization':`Basic ${base64.encode(basicAuth)}`
        },
        body: JSON.stringify(AgentCode)
      }).then(res =>res.json())
        .then(data => {
          res.status(200).json({
            type:type,
            subtitle:title,
            for:type,
            data:data
          })
        })
        .catch(err=>console.log(err))
      }


     function fetchPolicy(url,agentcode,title,req,res){
        let policycode={
          PolicyNumber:agentcode
        }
        fetch(url,{
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
            'Authorization':`Basic ${base64.encode(basicAuth)}`
          },
          body: JSON.stringify(policycode)
        }).then(res =>res.json())
          .then(data => {
            res.status(200).json({
              type:type,
              subtitle:title,
              for:type,
              data:data
            })
          })
          .catch(err=>console.log(err))
      }

     // fetching policy with response type filterable data
      function fetchPolicyDuePolicy(url,agentcode,title,req,res){
        let AgentCode={
          PolicyNumber:agentcode
        }
        fetch(url,{
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
            'Authorization':`Basic ${base64.encode(basicAuth)}`
          },
          body: JSON.stringify(AgentCode)
        }).then(res =>res.json())
          .then(data => {
            res.status(200).json({
              type:type,
              subtitle:title,
              for:type,
              data:data
            })
          })
          .catch(err=>console.log(err))
      }
 
// fetching Agent with response type filterable data
    function fetchAgentDuePolicy(url,agentcode,title,req,res){
      let AgentCode={
        AgentCode:agentcode
      }
      fetch(url,{
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization':`Basic ${base64.encode(basicAuth)}`
        },
        body: JSON.stringify(AgentCode)
      }).then(res =>res.json())
        .then(data => {
          res.status(200).json({
            type:type,
            subtitle:title,
            for:'policy_due',
            data:data
          })
        })
        .catch(err=>console.log(err))
    }
  


 
};
