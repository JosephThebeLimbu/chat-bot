const fetch = require('node-fetch');
const baseUrl = `${process.env.SOCKET_PROTOCOL}://${process.env.STRAPI_SERVER}:${process.env.STRAPI_PORT}`;

exports.create = function (req) {
    let url = baseUrl + '/feedbacks';
    
    return fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req)
    })
}