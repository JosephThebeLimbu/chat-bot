const fetch = require('node-fetch');
let host=process.env.SOCKET_PROTOCOL;
let server=process.env.SOCKET_HOST;
let port=process.env.port;
const baseUrl = `${host}://${server}:${port}/rest/v1/nccBranch`;

exports.getForex = function (req, res, next) {
  console.log("query", req.query.country);
  // support for both request with  headers or querys
  let country = req.sanitize(req.query.country) || req.sanitize(req.query.currency) || req.sanitize(req.headers.country) || req.sanitize(req.headers.currency);
  let countr = getCountry(country);
  fetchcountry(countr);

  function getCountry(matchingcode) {
    var usd = ['us', 'united states', 'America', 'america', 'dollar', 'usa'];
    var eur = ['europe', 'Europe', 'euro'];
    var aed = ['united arab', 'arab', 'united arab emirates', 'United Arab Emirates', 'dirham', 'AED'];
    var gbp = [
      'british',
      'british pound',
      'pound',
      'england',
      'uk',
      'united kingdom'
    ];
    var jpy = ['japanese yen', 'yen', 'japan'];
    var chf = ['swiss', 'swizerland'];
    var sgd = ['singapore', 'singapore dollar'];
    var aud = ['australian dollar', 'australia'];
    var qar = ['qatari riyal', 'qatar'];
    var dkk = ['danish krone', 'denmark', 'greenland'];
    var cad = ['canadian dollar', 'canada'];
    var hkd = ['hong kong dollar', 'hong kong'];
    var bhd = ['baraini dinar', 'bahrain'];
    var krw = ['south korean won', 'won', 'south korea'];
    var thb = ['thai bhat', 'thailand'];
    var sek = ['swedish krona', 'sweden'];
    var kwd = ['kuwaiti dinar', 'kuwait'];
    var myr = ['malaysian ringgit', 'malaysia'];
    var sar = ['saudi riyal', 'saudi', 'saudi arabia'];
    var cny = ['renminbi', 'china', 'republic of china', 'yuan'];
    var inr = ['india', 'indian rupee'];
    if (usd.includes(matchingcode.toLowerCase()) || matchingcode === "USD") {
      return 'USD';
    } else if (eur.includes(matchingcode.toLowerCase()) || matchingcode === "EUR") {
      return 'EUR';
    } else if (aed.includes(matchingcode.toLowerCase()) || matchingcode === "AED") {
      return 'AED';
    } else if (gbp.includes(matchingcode.toLowerCase()) || matchingcode === "GDP") {
      return 'GDP';
    } else if (jpy.includes(matchingcode.toLowerCase()) || matchingcode === "JPY") {
      return 'JPY';
    } else if (chf.includes(matchingcode.toLowerCase()) || matchingcode === "CHF") {
      return 'CHF';
    } else if (jpy.includes(matchingcode.toLowerCase()) || matchingcode === "JPY") {
      return 'JPY';
    } else if (aud.includes(matchingcode.toLowerCase()) || matchingcode === "AUD") {
      return 'AUD';
    } else if (sgd.includes(matchingcode.toLowerCase()) || matchingcode === "SGD") {
      return 'SGD';
    } else if (cad.includes(matchingcode.toLowerCase()) || matchingcode === "CAD") {
      return 'CAD';
    } else if (dkk.includes(matchingcode.toLowerCase()) || matchingcode === "DKK") {
      return 'DKK';
    } else if (qar.includes(matchingcode.toLowerCase()) || matchingcode === "QAR") {
      return 'QAR';
    } else if (sek.includes(matchingcode.toLowerCase()) || matchingcode === "SEK") {
      return 'SEK';
    } else if (thb.includes(matchingcode.toLowerCase()) || matchingcode === "THB") {
      return 'THB';
    } else if (krw.includes(matchingcode.toLowerCase()) || matchingcode === "KRW") {
      return 'KRW';
    } else if (bhd.includes(matchingcode.toLowerCase()) || matchingcode === "BHD") {
      return 'BHD';
    } else if (hkd.includes(matchingcode.toLowerCase()) || matchingcode === "HKD") {
      return 'HKD';
    } else if (inr.includes(matchingcode.toLowerCase()) || matchingcode === "INR") {
      return 'INR';
    } else if (cny.includes(matchingcode.toLowerCase()) || matchingcode === "CNY") {
      return 'CNY';
    } else if (sar.includes(matchingcode.toLowerCase()) || matchingcode === "SAR") {
      return 'SAR';
    } else if (myr.includes(matchingcode.toLowerCase()) || matchingcode === "MYR") {
      return 'MYR';
    } else if (kwd.includes(matchingcode.toLowerCase()) || matchingcode === "KWD") {
      return 'KWD';
    }
  }


  function fetchcountry(code) {
    let url = `${baseUrl}/forex?code=${code}`;
    let responseHeader = {};
    console.log("forex api call")
    fetch(url)
      .then(data => {
        responseHeader.status = data.status;
        return data.json();
      })
      .then((result) => {
        console.log("return data",result);
        let returnjson = renderForex(result);
        console.log("forex api val", result);
        result.header = responseHeader;
        res.status(200).json(returnjson);
      }).catch((err) => {
        let errorResponse = {
          message: `Sorry forexrate not available for ${country}`
        }
        res.status(500).json(errorResponse);
      });
  }


  function renderForex(forexData) {
    let forex = {
      type: 'forextable',
      title: `Exchange rate for the ${forexData.forex[0].currency_name}`,
      country: forexData.forex[0].currency_name,
      forexdate: forexData.forexdate,
      data: [{
        buying: forexData.forex[0].cash,
        selling: forexData.forex[0].selling,
      }]
    };
    return (forex);
  }
}