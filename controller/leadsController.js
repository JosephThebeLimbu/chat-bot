
const fetch = require("node-fetch");
const url = `http://${process.env.DASHBOARD_SERVER}:${process.env.DASHBOARD_PORT}/rest/v1/Organizations/${process.env.ORGANIZATION_ID}/leads`;

exports.postLeads = function(req, res, next) {
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: process.env.BOT_TOKEN
      },
      body: JSON.stringify({
        fullname: req.body.fullname,
        mobile_email: req.body.mobile,
        interest: req.body.interest,
        source: req.body.source,
        visitorId:req.body.visitorId,
        userId: process.env.USERID,
        location: req.body.location || ''
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log('posted', data);
        return res.status(200).json(data);
      })
      .catch(err => {
        console.log('Capturing server not respond yet || Not captures Lead');
        return false
      });
 
};
