const fetch = require('node-fetch');
let host=process.env.SOCKET_PROTOCOL;
let server=process.env.SOCKET_HOST;
let port=process.env.PORT;
const baseUrl = `${host}://${server}:${port}/rest/v1/nccBranch`;


exports.getLocation = function (req, res, next) {
    let type = req.sanitize(req.query.neartype) || req.sanitize(req.headers.neartype);
    console.log("headers",req.query);
    if(type=="undefined"){
        console.log(req.url,"requested url is")
        console.log("location type",type);
        let  latitude= req.sanitize(req.body.latitude);
        let longitude= req.sanitize(req.body.longitude);
        console.log(latitude,longitude,"latitutude,longi");
        let url = `${baseUrl}/branch?latitude=${latitude}&longitude=${longitude}`;
        console.log("fetch url",url);
        let responseHeader = {};   

        fetch(url).then(data => {
                responseHeader.status = data.status;
                return data.json();
            })
            .then((result) => {
                result.header = responseHeader;
                let outputDTO = {
                    header: responseHeader,
                    data: {}
                };

                console.log(result,"result")
    
                if(result==="No ATM data found" || result.length==0 || result===" No branches data found " || !result ) {
                    outputDTO.data = {
                        type: 'location',
                        title: `No Branch in this region`,
                        img:"images/compass.png",
                        subtitle: `No Branch at this location So for results near you, please send us your current location by clicking button below.`,
                        button: {
                            contents: [
                                {
                                    "title": "Type Location",
                                    "type": "type_location"
                                  }
                            ]
                          }
                    }
                    res.status(200).json(outputDTO.data);
                } 
                
                if (result.length) {           
                   console.log("branch",result);
                    outputDTO.data = {
                        type: 'generalslider',
                        title:'Branch location near you is in '+result[0].location+' area.',
                        data: result.map(elem => {
                            return {
                                'title': elem.BRANCH_NAME || elem.locationnp || elem.LOCATION +" Branch",
                                'people': {
                                    'role': elem.manager+ "",
                                     'phone': elem.phone
                                },
                                map: {
                                    latitude: elem.LATITUDE,
                                    longitude: elem.LONGITUDE
                                }
                            }
                        })
                    }
                    res.status(200).json(outputDTO.data);
                }
            }).catch((err) => {            
                let errorResponse = {
                    message: err.stack || err.message
                }
                res.status(500).json(errorResponse);
            });
    }

    else{
     console.log("atm");
    let type = req.sanitize(req.params.type);
    console.log("location type",type);
    let  latitude= req.sanitize(req.body.latitude);
    let longitude= req.sanitize(req.body.longitude);
    let url = `${baseUrl}/atm?latitude=${latitude}&longitude=${longitude}`;
    let responseHeader = {};    
    fetch(url)
        .then(data => {
            responseHeader.status = data.status;
            return data.json();
        })
        .then((result) => {
            result.header = responseHeader;
            let outputDTO = {
                header: responseHeader,
                data: {}
            };

            if(result==="No ATM data found" || result.length==0 || result===" No branches data found " || !result ) {
                outputDTO.data = {
                    type: 'location',
                    title: `No Branches in this region`,
                    img:"images/compass.png",
                    subtitle: `No  ATM this location So for results near you, please send us your current location by clicking button below.`,
                    button: {
                        contents: [
                            {
                                "title": "Type Location",
                                "type": "type_location"
                              }
                        ]
                      }
                }
                res.status(200).json(outputDTO.data);
            } 
            
            if (result.length) {
                outputDTO.data = {
                    type: 'generalslider',
                    title:'Branch location near you is in '+result[0].location+' area.',
                    data: result.map(elem => {
                        return {
                            'title': `${elem.BRANCH_NAME} (${location})`,
                            'people': {
                                'role': elem.manager+ "",
                                // 'Email': elem.branch_email
                            },
                            map: {
                                latitude: elem.LATITUDE,
                                longitude: elem.LONGITUDE
                            }
                        }
                    })
                }
                res.status(200).json(outputDTO.data);
            }
        }).catch((err) => {            
            let errorResponse = {
                message: err.stack || err.message
            }
            res.status(500).json(errorResponse);
        });
    }
}

exports.getAtmByLocation = function (req, res, next) {
    let location=req.sanitize(req.query.location) || req.sanitize(req.headers.location);
    console.log("ATM location",location);
    // let location = req.sanitize(req.params.location);
    let api1="atmapi";
    let url = `${baseUrl}/atmLocation?location=${location}`;
    // let url = `${baseUrl}/atm/${location}`;

    let responseHeader = {};
    fetch(url)
        .then(data => {
            responseHeader.status = data.status;
            return data.json()
        })
        .then((result) => {
            console.log("atm api",result);
            let outputDTO = {
                header: responseHeader,
                data: {}
            };

            if(result==="No ATM data found" || result.length==0 || result===" No ATM data found " || !result ){
                outputDTO.data = {
                    type: 'location',
                    imgtext:"No ATM at this location",
                    title: `No ATM in this region`,
                    notfoundimg:"images/compass.png",
                    subtitle: `For results near you, please send us your current location by clicking button below.`,
                    button: {
                        contents: [
                          {
                            title: "Send Location",
                            type: "send_location"
                          }
                        ]
                      }
                }
                res.status(200).json(outputDTO.data);
            }
            if (result.length) {
                outputDTO.data = {
                    type: 'generalslider',
                    title:`The available ATM in ${location}`,
                    data: result.map(elem => {
                        return {
                            'title': `${elem.BRANCH_NAME} (Location: ${location})`,
                            'people': {
                                'role': elem.manager+ "",
                                // 'Email': elem.branch_email
                            },
                            map: {
                                latitude: elem.LATITUDE,
                                longitude: elem.LONGITUDE
                            }
                        }
                    })
                }
                res.status(200).json(outputDTO.data);
            }
            else {
                outputDTO.data = {
                    type: 'location',
                    title: `No ATM  in this region`,
                    img:"images/compass.png",
                    subtitle: `No ATM in this location So for results near you, please send us your current location by clicking button below.`,
                    button: {
                        contents: [
                          {
                            title: "Send Location",
                            type: "send_location"
                          }
                        ]
                      }
                }
                res.status(200).json(outputDTO.data);
            }

           
        })
        .catch((err) => {
            let errorResponse = {
                message: err.stack || err.message
            }
            res.status(500).json(errorResponse);
        })
}



exports.getBranchesByLocation = function (req, res, next) {
    let location=req.sanitize(req.query.location) || req.sanitize(req.headers.location)
    console.log("branches location",location);
    // let location = req.sanitize(req.params.location);
     let url = `${baseUrl}/branchLocation?location=${location}`;

    let responseHeader = {};
    fetch(url)
        .then(data => {
            responseHeader.status = data.status;
            return data.json()
        })
        .then((result) => {
            result.header = responseHeader;

            let outputDTO = {
                header: responseHeader,
                data: {}
            };

            if(result==="No branch data found" || result.length==0 || result===" No branch data found " || !result ){
                outputDTO.data = {
                    type: 'location',
                    imgtext:"No Branches at this location",
                    title: `No Branches in this region`,
                    notfoundimg:"images/compass.png",
                    subtitle: `For results near you, please send us your current location by clicking button below.`,
                    button: {
                        contents: [
                          {
                            title: "Send Location",
                            type: "send_location"
                          }
                        ]
                      }
                }
                res.status(200).json(outputDTO.data);
            }

            else if (result.length) {
                outputDTO.data = {
                    type: 'generalslider',
                    title:`The available Brancahes in ${location}`,
                    data: result.map(elem => {
                        return {
                            'title': elem.BRANCH_NAME || elem.locationnp || elem.LOCATION +" Branch",
                                'people': {
                                    'role': elem.manager+ "",
                                     'phone': elem.phone
                                },
                            map: {
                                latitude: elem.LATITUDE,
                                longitude: elem.LONGITUDE
                            }
                        }
                    })
                }
            }
            res.status(200).json(outputDTO.data);
        })
        .catch((err) => {
            let errorResponse = {
                message: err.stack || err.message
            }
            // res.status(500).json(errorResponse);
        })
}


exports.getPublicIP = function (req, res, next) {
    let url = 'https://api.ipify.org?format=json';
    fetch(url)
        .then(data => data.json())
        .then((result) => {
            res.status(200).json(result)
        })
        .catch((err) => {
            res.status(500).json({
                'message': 'Error in getting public IP'
            })
        })
}


