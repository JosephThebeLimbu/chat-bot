/**
 * Source from https://nodemailer.com/smtp/
 */
var nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    secure: false,
    port: process.env.SMTP_PORT,
    auth: {
        user: process.env.EMAIL,
        pass: process.env.PASSWORD
    }
});

exports.sendMail = function (mailObj, config) {
    let mailStr = '';
        
    Object.keys(mailObj)    
    .forEach((key) => {        
        mailStr += key + ' : ' + mailObj[key] + '<br>';
    })

    let HelperOptions = {
        from: process.env.EMAIL,
        to: 'palmmindtest@gmail.com',
        subject: config.title,
        html: `${mailStr}`
    };

    return new Promise((res, rej) => {
        transporter.sendMail(HelperOptions, (error, info) => {            
            if (error) {
                rej(error);
            }
            else {
                res(info);
            }
        });
    })
}