
var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');

exports.postLeads = function(req, res, next) {
    console.log(req.body);
    console.log("selected");
    function submission(mailid,selected_catagories){
      let transporter = nodemailer.createTransport({
          host: process.env.SMTP_HOST,
          secure: false,
          port: process.env.SMTP_PORT,
         auth: {
              user:process.env.EMAIL ,
              pass: process.env.PASSWORD
          }
      });
      transporter.use('compile', hbs({
        viewEngine:"express-handlebars",
        viewPath:"./"
      }));
      let HelperOptions = {
          from: process.env.EMAIL,
          to: mailid,
          subject: "Nepal Bank Ltd.",
          template:"main",
          context: {
            subject:req.body.subject,
            name:req.body.fullname,
            email:req.body.email,
            mobile:req.body.mobile,
            problem:req.body.problem,
            catagories:selected_catagories
           }
        };

        if(req.headers.auth === process.env.salt_key){
          console.log("you have matching salt key",req.headers.auth);
          transporter.sendMail(HelperOptions, (error, info) => {            
            if (error) {
               console.log("mailconfig",error)
            }
            else {
              // console.log("info", info);
            //  res(info);
           res.status(200).json(HelperOptions);
            }
        })
        }
        else{
          return false
          console.log("Auth",error)
        }        
          

  }
  let selected_value=req.query.selected;
  console.log(req.query.selected);
  let mailid="";
  console.log("auth",req.headers.auth);
    switch(selected_value){
              case "ATM Card/ Visa Card":
                  mailid=process.env.ATM_Card_Visa_Card;
                  submission(mailid,selected_value);
                  break
              case "Mobile/Internet Banking":
                  mailid=process.env.Mobile_Internet_Banking;
                  submission(mailid,selected_value);
                  break
              case "Loan":
                  mailid=process.env.Loan;
                  submission(mailid,selected_value);
                  break
              case "Others":
                  mailid=Others;
                  submission(mailid,selected_value);
                  break
                  default:
                      return
          } 
 
};
