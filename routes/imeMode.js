const fetch = require("node-fetch");
const ImeModeController = require("../controller/ImeModeController");
const express = require("express");
const router = express.Router();

router.post("/", ImeModeController.postLeads);

module.exports = router;