const ImeModeController = require("../controller/ImeRivivalController");
const express = require("express");
const router = express.Router();

router.post("/", ImeModeController.postLeads);

module.exports = router;