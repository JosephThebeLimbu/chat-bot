const express = require('express');
const router = express.Router();
const locationController = require('../controller/locationController');

router.post('/type/', locationController.getLocation);
router.get('/ip', locationController.getPublicIP);
router.get('/atm/', locationController.getAtmByLocation);
router.get('/branch/', locationController.getBranchesByLocation);

module.exports = router;