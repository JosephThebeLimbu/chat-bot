const fetch = require("node-fetch");
const problemController = require("../controller/problemController");
const express = require("express");
const router = express.Router();

router.post("/", problemController.postLeads);

module.exports = router;